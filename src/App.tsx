import DataTable from './components/DataTable/DataTable';

function App() {
  const testData = [
    { id: 1, name: 'John Doe', age: 30, email: 'john@example.com' },
    { id: 2, name: 'Jane Smith', age: 25, email: 'jane@example.com' },
    { id: 3, name: 'Alice Johnson', age: 35, email: 'alice@example.com' },
    // Ajoutez plus de données si nécessaire
  ];
  const columns = [
    { key: 'id', label: 'ID' },
    { key: 'name', label: 'Name' },
    { key: 'age', label: 'Age' },
    { key: 'email', label: 'Email' }
  ];
  return (
    <div className="App">
      <h1>DataTable with React</h1>
      <DataTable data={testData} columns={columns} sortable={true} />
    </div>
  );
}

export default App;
